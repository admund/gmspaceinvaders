package me.admund.framework.android;

import android.os.Bundle;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import me.admund.framework.FrameworkTest;
import me.admund.gmspaceinvaders.GMSIGame;

public class AndroidLauncher extends AndroidApplication {

	//private AndroidAchivmentsProvider provider = null;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		//initialize(new FrameworkTest(null), config);
		initialize(new GMSIGame(null), config);
	}

//	@Override
//	protected void onStart() {
//		super.onStart();
//		provider.onStart(this);
//	}
//
//	@Override
//	protected void onStop() {
//		super.onStop();
//		provider.onStop();
//	}
//
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		super.onActivityResult(requestCode, resultCode, data);
//		provider.onActivityResult(requestCode, resultCode, data);
//	}
}
