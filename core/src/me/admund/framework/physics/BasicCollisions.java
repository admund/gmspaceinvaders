package me.admund.framework.physics;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

/**
 * Created by admund on 2015-01-11.
 */
public class BasicCollisions implements ContactListener {

    @Override
    public void beginContact(Contact contact) {
        Object objectA = PhysicsUtils.getObject(contact.getFixtureA().getBody());
        if(objectA != null && objectA instanceof PhysicsObject) {
            ((PhysicsObject)objectA).beginContact(contact, true);
        }

        Object objectB = PhysicsUtils.getObject(contact.getFixtureB().getBody());
        if(objectB != null && objectB instanceof PhysicsObject) {
            ((PhysicsObject)objectB).beginContact(contact, false);
        }
    }

    @Override
    public void endContact(Contact contact) {
        Object objectA = PhysicsUtils.getObject(contact.getFixtureA().getBody());
        if(objectA != null && objectA instanceof PhysicsObject) {
            ((PhysicsObject)objectA).endContact(contact, true);
        }

        Object objectB = PhysicsUtils.getObject(contact.getFixtureB().getBody());
        if(objectB != null && objectB instanceof PhysicsObject) {
            ((PhysicsObject)objectB).endContact(contact, false);
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {}

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {}
}

