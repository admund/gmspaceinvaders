package me.admund.framework.draw.holders;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import me.admund.framework.GameUtils;
import me.admund.framework.draw.animations.AnimationState;

/**
 * Created by admund on 2015-02-06.
 */
public class SimpleSpriteHolder extends AbstractSpriteHolder {

    public SimpleSpriteHolder(String textureName) {
        this(GameUtils.assetsManager.getTextureRegion(textureName));
    }

    public SimpleSpriteHolder(String textureAtlasName, String textureName) {
        this(GameUtils.assetsManager.getTextureRegion(textureAtlasName, textureName));
    }

    public SimpleSpriteHolder(TextureRegion textureRegion) {
        addSprite(new Sprite(textureRegion));
    }

    @Override
    public void changeAnimationState(AnimationState state) {}

    @Override
    public void act(float delta) {}
}
