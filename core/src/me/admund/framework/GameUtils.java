package me.admund.framework;

import me.admund.framework.assets.FrameworkAssetsManager;
import me.admund.framework.logic.Score;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.gmspaceinvaders.GMSIScore;
import me.admund.gmspaceinvaders.Sounds;
import me.admund.gmspaceinvaders.background.Wall;
import me.admund.gmspaceinvaders.enemies.EnemiesSpawner;
import me.admund.gmspaceinvaders.enemies.FlyingDeer;
import me.admund.gmspaceinvaders.player.Player;
import me.admund.gmspaceinvaders.scene.GameLogic;

/**
 * Created by admund on 2015-03-17.
 */
public class GameUtils {
    public static FrameworkAssetsManager assetsManager = null;

    public static Sounds sounds = null;
    public static GameLogic logic = null;
    public static GMSIScore score = null;

    public static Player player = null;
    public static FlyingDeer deer = null;

    public static EnemiesSpawner enemiesSpawner = null;
    public static PhysicsWorld world = null;
    public static Wall wall = null;
}
