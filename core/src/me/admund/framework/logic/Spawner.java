package me.admund.framework.logic;

import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by admund on 2015-03-18.
 */
public abstract class Spawner extends Actor {

    @Override
    public void act(float delta) {
        super.act(delta);

        update(delta);
        if(canSpawn()) {
            spawn();
        }
    }

    protected abstract void update(float deltaTime);
    protected abstract boolean canSpawn();
    protected abstract void spawn();
}
