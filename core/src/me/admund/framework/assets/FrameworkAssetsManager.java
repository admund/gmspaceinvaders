package me.admund.framework.assets;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import me.admund.gmspaceinvaders.assets.AssetsList;

/**
 * Created by admund on 2015-03-17.
 */
public class FrameworkAssetsManager extends AssetManager {
    private TextureAtlas mainAtlas = null;

    public void init() {
        mainAtlas = get(AssetsList.mainAtlasNamePNG, TextureAtlas.class);
    }

    public TextureRegion getTextureRegion(String fileName) {
        return mainAtlas.findRegion(fileName);
    }

    public TextureRegion getTextureRegion(String textereAtlasName, String fileName) {
        TextureAtlas atlas = get(textereAtlasName, TextureAtlas.class);
        return atlas.findRegion(fileName);
    }
}
