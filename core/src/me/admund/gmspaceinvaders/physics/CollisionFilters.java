package me.admund.gmspaceinvaders.physics;

/**
 * Created by admund on 2015-01-11.
 */
public class CollisionFilters {
    //public static final short DEFAULT = 0x0001;                   // 0000000000000001 in binary
    public static final short CATEGORY_PLAYER = 0x0002;             // 0000000000000010 in binary
    public static final short CATEGORY_WALL = 0x0040;               // 0000000001000000 in binary
    public static final short CATEGORY_ENEMY = 0x0004;              // 0000000000000100 in binary
    public static final short CATEGORY_LASER = 0x0008;              // 0000000000001000 in binary
    public static final short CATEGORY_DEER = 0x0010;             // 0000000000010000 in binary
//    public static final short CATEGORY_GROUND = 0x0020;           // 0000000000100000 in binary

    public static final short MASK_PLAYER = -1;
    public static final short MASK_WALL = -1;
    public static final short MASK_ENEMY = ~CATEGORY_ENEMY & ~CATEGORY_DEER;
    public static final short MASK_LASER = CATEGORY_ENEMY | CATEGORY_DEER;
    public static final short MASK_DEER = CATEGORY_LASER;
//    public static final short MASK_ENEMIES = CATEGORY_PLAYER | CATEGORY_BABY | CATEGORY_GROUND;
//    public static final short MASK_COIN = CATEGORY_PLAYER;
}
