package me.admund.gmspaceinvaders.physics;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import me.admund.framework.draw.DrawUtils;
import me.admund.framework.draw.holders.SimpleSpriteHolder;
import me.admund.framework.physics.PhysicsObject;
import me.admund.framework.physics.PhysicsUtils;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.gmspaceinvaders.assets.AssetsList;
import me.admund.gmspaceinvaders.enemies.Bang;
import me.admund.gmspaceinvaders.physics.CollisionFilters;

/**
 * Created by admund on 2015-03-17.
 */
public class WallElement extends PhysicsObject {
    public static final float SIZE_X = 12;
    public static final float SIZE_Y = 7;
    private static float START_POS_Y = PhysicsWorld.BOX_SCREEN_HEIGHT * .2f;

    @Override
    public BodyDef getBodyDef() {
        BodyDef def = new BodyDef();
        def.active = true;
        def.fixedRotation = true;
        def.type = BodyDef.BodyType.DynamicBody;
        def.gravityScale = 0;
        return def;
    }

    @Override
    public FixtureDef getFixtureDef() {
        FixtureDef def = new FixtureDef();
        def.shape = PhysicsUtils.getDefaultPolygonShape();
        def.density = 1000f;
        def.filter.categoryBits = CollisionFilters.CATEGORY_WALL;
        def.filter.maskBits = CollisionFilters.MASK_WALL;
        return def;
    }

    public void init(float posX) {
        super.init();
        setCurrentPos(posX, START_POS_Y);
        setSize(SIZE_X, SIZE_Y);
        setOrigin(Align.center);
        setSpriteHolder(new SimpleSpriteHolder(AssetsList.wall));
    }

    @Override
    public void beginContact(Contact contact, boolean isObjectA) {
        getInfo().setToReuse();
        Bang.createBang(getPosition(), getStage());
    }

    @Override
    public void endContact(Contact contact, boolean isObjectA) {}

    @Override
    public void draw(Batch batch, float parentAlpha) {
        DrawUtils.draw(batch, getSpriteList());
    }
}
