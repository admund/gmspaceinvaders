package me.admund.gmspaceinvaders.physics;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import me.admund.framework.GameUtils;
import me.admund.framework.draw.DrawObject;
import me.admund.framework.draw.DrawUtils;
import me.admund.framework.draw.holders.SimpleSpriteHolder;
import me.admund.framework.physics.PhysicsObject;
import me.admund.framework.physics.PhysicsUtils;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.gmspaceinvaders.assets.AssetsList;
import me.admund.gmspaceinvaders.background.GunFire;
import me.admund.gmspaceinvaders.enemies.Enemy;
import me.admund.gmspaceinvaders.gui.FloatingText;
import me.admund.gmspaceinvaders.player.HitCounter;

/**
 * Created by admund on 2015-03-19.
 */
public class Laser extends PhysicsObject {
    private float SIZE_X = 2;
    private float SIZE_Y = PhysicsWorld.BOX_SCREEN_HEIGHT;

    private float START_POS_Y = PhysicsWorld.BOX_SCREEN_HEIGHT * .5f;
    private float startPosXShift;

    private HitCounter hitCounter = null;

    private GunFire gunFire = null;

    //public void init(float startPosX, float startPosY, HitCounter hitCounter) {
    public void init(float startPosXShift, float startPosYShift, HitCounter hitCounter) {
        super.init();
        this.startPosXShift = startPosXShift;
        setCurrentPos(GameUtils.player.getX() + startPosXShift, GameUtils.player.getY() + START_POS_Y + startPosYShift);
        setSize(SIZE_X, SIZE_Y);
        setOrigin(Align.center);
        setSpriteHolder(new SimpleSpriteHolder(AssetsList.laser));
        setColor(Color.WHITE);
        addAction(createAction());

        createFire(GameUtils.player.getX() + startPosXShift, GameUtils.player.getY() + startPosYShift);

        this.hitCounter = hitCounter;
    }

    @Override
    public BodyDef getBodyDef() {
        BodyDef def = new BodyDef();
        def.active = true;
        def.fixedRotation = true;
        def.type = BodyDef.BodyType.DynamicBody;
        def.gravityScale = 0;
        return def;
    }

    @Override
    public FixtureDef getFixtureDef() {
        FixtureDef def = new FixtureDef();
        def.shape = PhysicsUtils.getDefaultPolygonShape();
        def.density = 1000f;
        def.filter.categoryBits = CollisionFilters.CATEGORY_LASER;
        def.filter.maskBits = CollisionFilters.MASK_LASER;
        return def;
    }

    @Override
    public void beginContact(Contact contact, boolean isObjectA) {
        Object obj = getContactedObject(contact, isObjectA);

        if(obj instanceof Enemy) {
            hitCounter.addHit((Enemy)obj);
        }
    }

    @Override
    public void endContact(Contact contact, boolean isObjectA) {}

    @Override
    public void act(float delta) {
        super.act(delta);
        updatePos();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        DrawUtils.draw(batch, getSpriteList());
    }

    private Action createAction() {
        return Actions.sequence(
                Actions.delay(.1f),
                new Action() {
                    @Override
                    public boolean act(float delta) {
                        getBody().setActive(false);
                        return true;
                    }
                },
                Actions.fadeOut(.3f),//.5f),
                new Action() {
                    @Override
                    public boolean act(float delta) {
                        getInfo().setToReuse();

                        int score = GameUtils.score.calcScore(hitCounter.getHitCnt());
                        GameUtils.score.addScore(score);
                        if(score > 0) {
                            createFloatingPts(score);
                        }
                        hitCounter.clear();
                        return true;
                    }
                });
    }

    private void createFloatingPts(int pts) {
        FloatingText text = new FloatingText("+" + pts);
        text.init(getX(), getY() + getHeight() * .5f);
        getStage().addActor(text);
    }

    private void createFire(float startPosX, float startPosY) {
        gunFire = new GunFire();
        gunFire.init(startPosX, startPosY);
        getStage().addActor(gunFire);
    }

    private void updatePos() {
        setCurrentPos(GameUtils.player.getX() + startPosXShift, getY() + SIZE_Y * .5f);
        gunFire.setPosition(GameUtils.player.getX() - SIZE_X * 2 + startPosXShift, getY());
    }

}
