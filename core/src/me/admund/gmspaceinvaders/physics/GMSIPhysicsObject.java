package me.admund.gmspaceinvaders.physics;

import me.admund.framework.GameUtils;
import me.admund.framework.physics.PhysicsObject;

/**
 * Created by admund on 2015-01-18.
 */
public abstract class GMSIPhysicsObject extends PhysicsObject {

    @Override
    public void act(float delta) {
        super.act(delta);
        if(!canBeReuse() && (getY() < 0)) {
            prepereToReuse();

            GameUtils.logic.stopGame();
        }
    }
}
