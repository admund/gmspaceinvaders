package me.admund.gmspaceinvaders.physics;

import me.admund.framework.physics.PhysicsObject;
import me.admund.framework.physics.ReuseFactory;
import me.admund.gmspaceinvaders.enemies.Enemy;
import me.admund.gmspaceinvaders.enemies.FlyingDeer;
import me.admund.gmspaceinvaders.player.Player;

/**
 * Created by admund on 2015-01-18.
 */
public class GMSIReuseFactory extends ReuseFactory {

    @Override
    public PhysicsObject createNewObj(String className) {
        PhysicsObject obj = null;
        if(className.equals(Player.class.getName())) {
            obj = new Player();
        } else if(className.equals(FlyingDeer.class.getName())) {
            obj = new FlyingDeer();
        }else if(className.equals(WallElement.class.getName())) {
            obj = new WallElement();
        } else if(className.equals(Enemy.class.getName())) {
            obj = new Enemy();
        } else if(className.equals(Laser.class.getName())) {
            obj = new Laser();
        }
        return obj;
    }
}
