package me.admund.gmspaceinvaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import me.admund.framework.GameUtils;
import me.admund.framework.achievements.IAchievementsProvider;
import me.admund.framework.game.AbstractGame;
import me.admund.framework.scenes.ScenesManager;
import me.admund.gmspaceinvaders.assets.AssetsList;
import me.admund.gmspaceinvaders.assets.GMISAssetManager;
import me.admund.gmspaceinvaders.scene.GameScene;

/**
 * Created by admund on 2015-03-17.
 */
public class GMSIGame extends AbstractGame {

    public GMSIGame(IAchievementsProvider achievementsProvider) {
        super(achievementsProvider);
    }

    @Override
    public void load() {
        GameUtils.assetsManager = new GMISAssetManager();
        GameUtils.assetsManager.load(AssetsList.mainAtlasNamePNG, TextureAtlas.class);

        GameUtils.assetsManager.finishLoading();
        GameUtils.assetsManager.init();

        GameUtils.sounds = new Sounds();
        GameUtils.sounds.init();
    }

    @Override
    public void create() {
        super.create();
        ScenesManager.inst().push(new GameScene(), true);

        GameUtils.sounds.playMain();
    }

    @Override
    public void render() {
        super.render();
    }
}
