package me.admund.gmspaceinvaders.scene;

import me.admund.framework.GameUtils;

/**
 * Created by admund on 2015-03-23.
 */
public class GameLogic {

    private boolean gameStarted = false;

    public boolean isStarted() {
        return gameStarted;
    }

    public void stopGame() {
        if(gameStarted) {
            gameStarted = false;

            GameUtils.score.updateBestScore();

            GameUtils.enemiesSpawner.autodestrucAllEnemies();
        }
    }

    public void restartGame() {
        if(!gameStarted) {
            gameStarted = true;

            GameUtils.wall.init();
            GameUtils.player.init();
            GameUtils.deer.restart();

            GameUtils.score.save();
        }
    }

}
