package me.admund.gmspaceinvaders.scene;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import me.admund.framework.GameUtils;
import me.admund.framework.draw.DrawObject;
import me.admund.framework.draw.holders.SimpleSpriteHolder;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.framework.scenes.AbstractScene;
import me.admund.gmspaceinvaders.GMSIGamePreferences;
import me.admund.gmspaceinvaders.GMSIScore;
import me.admund.gmspaceinvaders.background.Wall;
import me.admund.gmspaceinvaders.enemies.FlyingDeer;
import me.admund.gmspaceinvaders.assets.AssetsList;
import me.admund.gmspaceinvaders.background.Background;
import me.admund.gmspaceinvaders.enemies.EnemiesSpawner;
import me.admund.gmspaceinvaders.gui.Gui;
import me.admund.gmspaceinvaders.physics.GMSIReuseFactory;
import me.admund.gmspaceinvaders.player.Player;

/**
 * Created by admund on 2015-03-17.
 */
public class GameScene extends AbstractScene {

    @Override
    public void create() {
        createBackground();
        createWorld();

        createPlayer();
        createDeer();

        createSpawner();
        createWalls();

        createLogic();
        createGUI();
    }

    @Override
    public void act(float deltaTime) {
        GameUtils.world.step(deltaTime);
        super.act(deltaTime);
    }

    @Override
    public void draw(Batch batch) {
        super.draw(batch);
        //GameUtils.world.debugRender(getCurrentCamera());
    }

    private void createBackground() {
        DrawObject background = new Background();
        background.setSpriteHolder(new SimpleSpriteHolder(AssetsList.background));
        background.setSize(PhysicsWorld.BOX_SCREEN_WIDTH, PhysicsWorld.BOX_SCREEN_HEIGHT);
        stage.addActor(background);
    }

    private void createWorld() {
        GameUtils.world = new PhysicsWorld(new GMSIReuseFactory());
    }

    private void createPlayer() {
        GameUtils.player = (Player) GameUtils.world.getPhysicsObject(Player.class.getName());
        stage.addActor(GameUtils.player);
        GameUtils.player.init();
    }

    private void createDeer() {
        GameUtils.deer = (FlyingDeer) GameUtils.world.getPhysicsObject(FlyingDeer.class.getName());
        GameUtils.deer.init();
        stage.addActor(GameUtils.deer);
    }

    private void createSpawner() {
        GameUtils.enemiesSpawner = new EnemiesSpawner();
        stage.addActor(GameUtils.enemiesSpawner);
    }

    private void createWalls() {
        GameUtils.wall = new Wall();
        stage.addActor(GameUtils.wall );
        GameUtils.wall.init();
    }

    private void createLogic() {
        GameUtils.logic = new GameLogic();
    }

    private void createGUI() {
        GameUtils.score = new GMSIScore();
        GameUtils.score.init(new GMSIGamePreferences(Gdx.app.getPreferences("GMSI")));

        guiStage.addActor(new Gui(this));
    }
}
