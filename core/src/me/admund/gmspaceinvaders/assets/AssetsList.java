package me.admund.gmspaceinvaders.assets;

/**
 * Created by admund on 2015-03-17.
 */
public class AssetsList {

    public static String mainAtlasNamePNG = "all.atlas";

    // ATALS ELEMENTS
    public static String background = "ui_bg_main_tile";

    public static String before_fill = "before_fill";
    public static String fill = "fill";
    public static String back = "back";

    public static String player = "game_character_hero";
    public static String laser = "game_fx_projectile_laser";
    public static String gun_fire = "game_fx_gunfire";
    public static String engine_fire = "game_fx_engine";

    public static String deer = "game_character_special_spacedeer";

    public static String[] enemies = {"game_character_enemy_1",
                                    "game_character_enemy_2",
                                    "game_character_enemy_3",
                                    "game_character_enemy_4"};

    public static String wall = "game_obstacle_barrier";

    public static String bang = "game_fx_explosion";

}
