package me.admund.gmspaceinvaders.enemies;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import me.admund.framework.physics.PhysicsWorld;

/**
 * Created by admund on 2015-03-23.
 */
public class FlyingDeerController {

    private FlyingDeer deer = null;

    private boolean directionRight = false;
    private boolean flipedRight = true;

    private float horizontalSpeed = 30f;
    private float horizontalSpeedStep = 5f;

    private int speedLevel = -1;

    public void act(float deltaTime) {
        if(!directionRight && deer.getX() < -PhysicsWorld.BOX_SCREEN_WIDTH) {
            changeDirection();
        } else if(directionRight && deer.getX() > PhysicsWorld.BOX_SCREEN_WIDTH * 2) {
            changeDirection();
        }
    }

    public void restart() {
        speedLevel = 0;
    }

    public Vector2 randomStartPos() {
        float posY = PhysicsWorld.BOX_SCREEN_HEIGHT * .5f + PhysicsWorld.BOX_SCREEN_HEIGHT * .4f * MathUtils.random();

        float posX;
        directionRight = MathUtils.randomBoolean();
        if(directionRight) {
            posX = -PhysicsWorld.BOX_SCREEN_WIDTH * 3;
        } else {
            posX = PhysicsWorld.BOX_SCREEN_WIDTH * 4;
        }
        return new Vector2(posX, posY);
    }

    public void init(FlyingDeer deer) {
        this.deer = deer;
        speedLevel++;
        deer.getBody().setLinearVelocity(getCurrentSpeed(), 0);
        flip();
    }

    private float getCurrentSpeed() {
        return (directionRight ? 1 : -1) * (horizontalSpeed + horizontalSpeedStep * speedLevel);
    }

    private void changeDirection() {
        directionRight = !directionRight;
        deer.getBody().setLinearVelocity(getCurrentSpeed(), 0);
        flip();
    }

    private void flip() {
        if(directionRight && !flipedRight) {
            deer.flip(true, false);
            flipedRight = !flipedRight;
        } else if(!directionRight && flipedRight) {
            deer.flip(true, false);
            flipedRight = !flipedRight;
        }
    }
}
