package me.admund.gmspaceinvaders.enemies;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import me.admund.framework.GameUtils;
import me.admund.framework.draw.DrawUtils;
import me.admund.framework.draw.holders.SimpleSpriteHolder;
import me.admund.framework.physics.PhysicsObject;
import me.admund.framework.physics.PhysicsUtils;
import me.admund.gmspaceinvaders.assets.AssetsList;
import me.admund.gmspaceinvaders.gui.FloatingText;
import me.admund.gmspaceinvaders.physics.CollisionFilters;

/**
 * Created by admund on 2015-03-23.
 */
public class FlyingDeer extends PhysicsObject {
    public static final float SIZE_X = 6;
    public static final float SIZE_Y = 10;

    private FlyingDeerController controller = new FlyingDeerController();

    private boolean isDead = false;

    public void init() {
        super.init();
        isDead = false;
        setSize(SIZE_X, SIZE_Y);
        setCurrentPos(controller.randomStartPos());
        setOrigin(Align.center);
        setSpriteHolder(new SimpleSpriteHolder(AssetsList.deer));

        controller.init(this);
    }

    public void restart() {
        controller.restart();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        controller.act(delta);

        if(isDead) {
            init();
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        DrawUtils.draw(batch, getSpriteList());
    }

    @Override
    public void beginContact(Contact contact, boolean isObjectA) {
        isDead = true;
        getInfo().setToReuse();
        Bang.createBang(getPosition(), getStage());

        GameUtils.score.addScore(666);
        createFloatingPts(666);
    }

    @Override
    public void endContact(Contact contact, boolean isObjectA) {}

    @Override
    public BodyDef getBodyDef() {
        BodyDef def = new BodyDef();
        def.active = true;
        def.fixedRotation = true;
        def.type = BodyDef.BodyType.DynamicBody;
        def.gravityScale = 0;
        return def;
    }

    @Override
    public FixtureDef getFixtureDef() {
        FixtureDef def = new FixtureDef();
        def.shape = PhysicsUtils.getDefaultPolygonShape();
        def.density = 1;//000f;
        def.filter.categoryBits = CollisionFilters.CATEGORY_DEER;
        def.filter.maskBits = CollisionFilters.MASK_DEER;
        return def;
    }

    private void createFloatingPts(int pts) {
        FloatingText text = new FloatingText("+" + pts);
        text.init(getX(), getY());// + getHeight() * .5f);
        getStage().addActor(text);
    }
}
