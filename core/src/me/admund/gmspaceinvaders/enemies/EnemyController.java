package me.admund.gmspaceinvaders.enemies;

import com.badlogic.gdx.math.MathUtils;
import me.admund.framework.physics.PhysicsWorld;

/**
 * Created by admund on 2015-03-18.
 */
public class EnemyController {
    public static float MAX_RANGE = PhysicsWorld.BOX_SCREEN_WIDTH * .1f;

    private Enemy enemy = null;

    private float horizontalSpeed = 25f;
    private float verticalSpeed = -15f;

    private float enemyStartPosX = 0;
    private float currentRange = MAX_RANGE * (.5f + MathUtils.random(.5f));

    public EnemyController(Enemy enemy) {
        this.enemy = enemy;
    }

    public void init(float enemyStartPosX) {
        this.enemyStartPosX = enemyStartPosX;
        enemy.getBody().setLinearVelocity(horizontalSpeed * MathUtils.randomSign(), verticalSpeed);
    }

    public void update(float deltaTime) {
        if(enemyStartPosX - currentRange > enemy.getX()) {
            enemy.getBody().setLinearVelocity(horizontalSpeed, verticalSpeed);
        } else if(enemyStartPosX + currentRange < enemy.getX()) {
            enemy.getBody().setLinearVelocity(-horizontalSpeed, verticalSpeed);
        }
    }
}
