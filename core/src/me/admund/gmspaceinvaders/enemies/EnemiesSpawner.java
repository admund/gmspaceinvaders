package me.admund.gmspaceinvaders.enemies;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import me.admund.framework.GameUtils;
import me.admund.framework.logic.Spawner;
import me.admund.framework.physics.PhysicsWorld;

/**
 * Created by admund on 2015-03-18.
 */
public class EnemiesSpawner extends Spawner {

    private float SPAWN_DELAY = 3f;
    private float lastSpawn = 2f;

    private float spwnerStep = 0;
    private float spawnerLevel = 1;

    private Array<Enemy> enemyArray = new Array<Enemy>();

    @Override
    protected void update(float deltaTime) {
        lastSpawn -= deltaTime;
    }

    public void autodestrucAllEnemies() {
        for(int i=0; i<enemyArray.size; i++) {
            enemyArray.get(i).destroySelf();
        }
        restart();
    }

    @Override
    protected boolean canSpawn() {
        if(GameUtils.logic.isStarted() && lastSpawn < 0) {
            lastSpawn = SPAWN_DELAY - spwnerStep * .2f;
            return true;
        }
        return false;
    }

    @Override
    protected void spawn() {
        for(int i=0; i<spawnerLevel; i++) {
            getStage().addActor(createEnemy());
        }

        spwnerStep++;
        if(spwnerStep == 10) {
            spwnerStep = 0;
            spawnerLevel++;
        }
    }

    private Enemy createEnemy() {
        Enemy enemy = (Enemy) GameUtils.world.getPhysicsObject(Enemy.class.getName());
        enemy.init(getStartPosX());
        if(!enemyArray.contains(enemy, true)) {
            enemyArray.add(enemy);
        }
        return enemy;
    }

    private float getStartPosX() {
        return MathUtils.random() * PhysicsWorld.BOX_SCREEN_WIDTH;
    }

    private void restart() {
        spwnerStep = 0;
        spawnerLevel = 1;
    }
}
