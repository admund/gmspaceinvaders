package me.admund.gmspaceinvaders.enemies;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import me.admund.framework.GameUtils;
import me.admund.framework.draw.DrawObject;
import me.admund.framework.draw.DrawUtils;
import me.admund.framework.draw.holders.SimpleSpriteHolder;
import me.admund.gmspaceinvaders.assets.AssetsList;

/**
 * Created by admund on 2015-03-18.
 */
public class Bang extends DrawObject {
    public static final float SIZE_X = 20;
    public static final float SIZE_Y = 20;

    public static void createBang(Vector2 pos, Stage stage) {
        Bang bang = new Bang();
        bang.init(pos);
        stage.addActor(bang);

        GameUtils.sounds.playExplosion();
    }

    public void init(Vector2 enemyPossition) {
        setSize(SIZE_X, SIZE_Y);
        setPosition(enemyPossition.x - SIZE_X * .5f, enemyPossition.y - SIZE_Y * .5f);
        setOrigin(Align.center);
        setSpriteHolder(new SimpleSpriteHolder(AssetsList.bang));

        setScale(.1f);
        addAction(createStartAction());
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        DrawUtils.draw(batch, getSpriteList());
    }

    private Action createStartAction() {
        return Actions.sequence(
                Actions.scaleTo(1, 1, .1f),
                Actions.fadeOut(.2f),
                new Action() {
                    @Override
                    public boolean act(float delta) {
                        remove();
                        return true;
                    }
                });
    }
}
