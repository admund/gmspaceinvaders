package me.admund.gmspaceinvaders.enemies;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import me.admund.framework.GameUtils;
import me.admund.framework.draw.DrawUtils;
import me.admund.framework.draw.holders.SimpleSpriteHolder;
import me.admund.framework.physics.PhysicsUtils;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.gmspaceinvaders.assets.AssetsList;
import me.admund.gmspaceinvaders.physics.CollisionFilters;
import me.admund.gmspaceinvaders.physics.GMSIPhysicsObject;

/**
 * Created by admund on 2015-03-18.
 */
public class Enemy extends GMSIPhysicsObject {
    private static float START_POS_Y = PhysicsWorld.BOX_SCREEN_HEIGHT * 1.1f;

    private EnemyController controller = null;

    public Enemy() {
        controller = new EnemyController(this);
    }

    public void init(float startPosX) {
        super.init();
        startPosX = correctionStartPos(startPosX);
        setCurrentPos(startPosX, START_POS_Y);
        setOrigin(Align.center);
        TextureRegion enemyTexture = getRandomEnemyTexture();
        setSize(enemyTexture.getRegionWidth() * .1f,
                enemyTexture.getRegionHeight() * .1f);
        setSpriteHolder(new SimpleSpriteHolder(enemyTexture));

        controller.init(startPosX);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        controller.update(delta);
    }

    @Override
    public BodyDef getBodyDef() {
        BodyDef def = new BodyDef();
        def.active = true;
        def.fixedRotation = true;
        def.type = BodyDef.BodyType.DynamicBody;
        def.gravityScale = 0;
        return def;
    }

    @Override
    public FixtureDef getFixtureDef() {
        FixtureDef def = new FixtureDef();
        def.shape = PhysicsUtils.getDefaultPolygonShape();
        def.density = 1;//000f;
        def.filter.categoryBits = CollisionFilters.CATEGORY_ENEMY;
        def.filter.maskBits = CollisionFilters.MASK_ENEMY;
        return def;
    }

    @Override
    public void beginContact(Contact contact, boolean isObjectA) {
        Object obj = getContactedObject(contact, isObjectA);

        contact.setEnabled(false);

        destroySelf();
    }

    @Override
    public void endContact(Contact contact, boolean isObjectA) {}

    @Override
    public void draw(Batch batch, float parentAlpha) {
        DrawUtils.draw(batch, getSpriteList());
    }

    private TextureRegion getRandomEnemyTexture() {
        return GameUtils.assetsManager.getTextureRegion(AssetsList.enemies[MathUtils.random(AssetsList.enemies.length-1)]);
    }

    public void destroySelf() {
        getInfo().setToReuse();
        Bang.createBang(getPosition(), getStage());
    }

    private float correctionStartPos(float startPosX) {
        if(startPosX < controller.MAX_RANGE*2) {
            return controller.MAX_RANGE*2;
        } else if(startPosX > (PhysicsWorld.BOX_SCREEN_WIDTH - controller.MAX_RANGE*2)) {
            return PhysicsWorld.BOX_SCREEN_WIDTH - controller.MAX_RANGE*2;
        }
        return startPosX;
    }
}
