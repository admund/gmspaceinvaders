package me.admund.gmspaceinvaders;

import me.admund.framework.logic.Score;

/**
 * Created by admund on 2015-03-23.
 */
public class GMSIScore extends Score {
    public static final String SCORE = "SCORE";
    public static final String BEST_SCORE = "BEST_SCORE";

    private static final int basePts = 10;

    public int calcScore(int cnt) {
        return basePts * cnt * cnt;
    }

    public void addScore(int score) {
        addScore(SCORE, score);
    }

    public int getScore() {
        return (int)getScore(SCORE);
    }

    public int getBestScore() {
        return (int)getScore(BEST_SCORE);
    }

    @Override
    public void save() {
        updateBestScore();
        setScore(SCORE, 0);
        super.save();
    }

    public void updateBestScore() {
        int score = getScore();
        int bestScore = getBestScore();
        if(score > bestScore) {
            setScore(BEST_SCORE, score);
        }
    }
}
