package me.admund.gmspaceinvaders;

import com.badlogic.gdx.Preferences;
import me.admund.framework.GameUtils;
import me.admund.framework.logic.GamePreferences;
import me.admund.framework.logic.Score;

/**
 * Created by admund on 2015-03-23.
 */
public class GMSIGamePreferences extends GamePreferences {

    public GMSIGamePreferences(Preferences preferences) {
        super(preferences);
    }

    @Override
    public void init() {
        GameUtils.score.addScore(GMSIScore.BEST_SCORE, preferences.getInteger(GMSIScore.BEST_SCORE, 0));
    }

    @Override
    public void save(Score score) {
        System.out.println("save " + (int)score.getScore(GMSIScore.BEST_SCORE));
        preferences.putInteger(GMSIScore.BEST_SCORE, (int)score.getScore(GMSIScore.BEST_SCORE));
        preferences.flush();
    }
}
