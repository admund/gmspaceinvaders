package me.admund.gmspaceinvaders.gui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import me.admund.gmspaceinvaders.scene.GameScene;

/**
 * Created by admund on 2015-02-16.
 */
public class Gui extends Actor {
    private GameScene scene = null;

    private ScoreGui score = null;

    public Gui(GameScene scene) {
        this.scene = scene;
    }

    @Override
    protected void setStage(Stage stage) {
        super.setStage(stage);
        if(stage != null) {
            createScoreGui(stage);
            createProgressBar(stage);
        }
    }

    private void createScoreGui(Stage guiStage) {
        score = new ScoreGui();
        guiStage.addActor(score);
    }

    private void createProgressBar(Stage guiStage) {
        guiStage.addActor(new GMSIProgressBar());
    }

    @Override
    public void act(float delta) {
        score.act(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        score.draw(batch, parentAlpha);
    }
}
