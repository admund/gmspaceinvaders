package me.admund.gmspaceinvaders.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import me.admund.framework.draw.DrawObject;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.framework.utils.FontUtils;

/**
 * Created by admund on 2015-03-04.
 */
public class FloatingText extends DrawObject {
    private static float changePosY = 10f;
    private static float animationTime = 1.5f;

    private BitmapFont font;
    private String text;

    public FloatingText(String text) {
        this.text = text;
        this.font = FontUtils.getBasicFont();
    }

    public void init(float posX, float posY) {
        setPosition(posX/* + 50*/, posY);
        addAction(createActions());
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if(getColor().a == 0) {
            remove();
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        Color tmp = font.getColor();
        font.setColor(getColor());
        font.draw(batch, text, getX() * PhysicsWorld.BOX_TO_SCREEN, getY() * PhysicsWorld.BOX_TO_SCREEN);
        font.setColor(tmp);
    }

    private Action createActions() {
        return Actions.parallel(
                Actions.moveBy(0, changePosY, animationTime),
                Actions.fadeOut(animationTime));
    }
}
