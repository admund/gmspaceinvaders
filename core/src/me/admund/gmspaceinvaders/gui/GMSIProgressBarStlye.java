package me.admund.gmspaceinvaders.gui;

import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import me.admund.framework.draw.gui.GuiUtils;
import me.admund.gmspaceinvaders.assets.AssetsList;

/**
 * Created by admund on 2015-03-19.
 */
public class GMSIProgressBarStlye extends ProgressBar.ProgressBarStyle {

    public static ProgressBar.ProgressBarStyle create() {
        GMSIProgressBarStlye style = new GMSIProgressBarStlye();
        style.background = GuiUtils.createSpriteDrawable(AssetsList.back);
        style.knob = GuiUtils.createSpriteDrawable(AssetsList.fill);
        style.knobBefore = GuiUtils.createSpriteDrawable(AssetsList.before_fill);
        return style;
    }

}
