package me.admund.gmspaceinvaders.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import me.admund.framework.GameUtils;
import me.admund.framework.utils.FontUtils;

/**
 * Created by admund on 2015-02-23.
 */
public class ScoreGui extends Actor {
    private float screenWidth = Gdx.graphics.getWidth();
    private float screenHeight = Gdx.graphics.getHeight();

    @Override
    public void draw(Batch batch, float parentAlpha) {
        drawDebugGUI(batch);
    }

    public void drawDebugGUI(Batch batch) {
        //Color tmp = batch.getColor();
        //batch.setColor(Color.CYAN);

        FontUtils.getBasicFont().setColor(Color.rgb888(.15f, 1, .08f));//Color.OLIVE);
        FontUtils.getBasicFont().draw(batch, "FPS " + Gdx.graphics.getFramesPerSecond(),
                screenWidth - 100, 20);

        if(GameUtils.logic.isStarted()) {

            FontUtils.getBasicFont().draw(batch, "Score - " + GameUtils.score.getScore() + " pts",
                    40, screenHeight - 10);

        } else {
            FontUtils.getBasicFont().draw(batch, "Last Score - " + GameUtils.score.getScore() + " pts",
                    screenWidth * .5f, screenHeight * .5f + 30);

            FontUtils.getBasicFont().draw(batch, "Best Score - " + GameUtils.score.getBestScore() + " pts",
                    screenWidth * .5f, screenHeight * .5f);

            FontUtils.getBasicFont().draw(batch, "press ENTER to restart",
                    screenWidth * .5f, screenHeight * .5f - 30);
        }

        //batch.setColor(tmp);
    }
}
