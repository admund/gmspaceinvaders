package me.admund.gmspaceinvaders.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import me.admund.framework.GameUtils;
import me.admund.framework.physics.PhysicsWorld;

/**
 * Created by admund on 2015-03-23.
 */
public class GMSIProgressBar extends ProgressBar {

    private static final int MAX = 100;

    public GMSIProgressBar() {
        super(0, 100, 1, false, GMSIProgressBarStlye.create());

        //setPosition((PhysicsWorld.BOX_SCREEN_WIDTH - PhysicsWorld.BOX_SCREEN_WIDTH * .3f) * PhysicsWorld.BOX_TO_SCREEN,
        //        (PhysicsWorld.BOX_SCREEN_HEIGHT - PhysicsWorld.BOX_SCREEN_HEIGHT * .1f) * PhysicsWorld.BOX_TO_SCREEN);
        setPosition(Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * .3f,
               Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * .1f);

        setSize(PhysicsWorld.BOX_SCREEN_WIDTH * .2f * PhysicsWorld.BOX_TO_SCREEN,
                PhysicsWorld.BOX_SCREEN_HEIGHT * .1f * PhysicsWorld.BOX_TO_SCREEN);
    }

    @Override
    public void act(float delta) {
        setValue(GameUtils.player.getReloadTime() * MAX);
    }
}
