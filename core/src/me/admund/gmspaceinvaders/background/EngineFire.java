package me.admund.gmspaceinvaders.background;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import me.admund.framework.draw.DrawObject;
import me.admund.framework.draw.DrawUtils;
import me.admund.framework.draw.holders.SimpleSpriteHolder;
import me.admund.framework.physics.PhysicsObject;
import me.admund.gmspaceinvaders.assets.AssetsList;

/**
 * Created by admund on 2015-03-23.
 */
public class EngineFire extends DrawObject {
    private float SIZE_X = 5;
    private float SIZE_Y = 5;

    private float posXShift;

    private PhysicsObject obj = null;

    public void init(PhysicsObject obj, float posX, float posY) {
        this.obj = obj;
        this.posXShift = posX;
        setSize(SIZE_X, SIZE_Y);
        setPosition(obj.getX() + posXShift, posY);
        setOrigin(SIZE_X * .5f, SIZE_Y);

        setSpriteHolder(new SimpleSpriteHolder(AssetsList.engine_fire));
        addAction(createAction());
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        updatePos();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        DrawUtils.draw(batch, getSpriteList());
    }

    public void updatePos() {
        setPosition(obj.getX() + posXShift, getY());
    }

    private Action createAction() {
        return Actions.repeat(RepeatAction.FOREVER,
                Actions.sequence(Actions.scaleTo(.5f, .5f, .5f),
                        Actions.scaleTo(1f, 1f, .5f)));
    }
}
