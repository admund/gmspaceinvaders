package me.admund.gmspaceinvaders.background;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import me.admund.framework.draw.DrawObject;
import me.admund.framework.draw.DrawUtils;
import me.admund.framework.draw.holders.SimpleSpriteHolder;
import me.admund.gmspaceinvaders.assets.AssetsList;

/**
 * Created by admund on 2015-03-23.
 */
public class GunFire extends DrawObject {

    private float SIZE_X = 8;
    private float SIZE_Y = 8;

    public void init(float posX, float posY) {
        setSize(SIZE_X, SIZE_Y);
        setPosition(posX - SIZE_X * .5f, posY);

        setSpriteHolder(new SimpleSpriteHolder(AssetsList.gun_fire));

        addAction(createAction());
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        DrawUtils.draw(batch, getSpriteList());
    }

    private Action createAction() {
        return Actions.sequence(
                Actions.fadeOut(.3f),
                new Action() {
                    @Override
                    public boolean act(float delta) {
                        remove();

                        return true;
                    }
                });
    }
}
