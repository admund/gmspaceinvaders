package me.admund.gmspaceinvaders.background;

import com.badlogic.gdx.graphics.g2d.Batch;
import me.admund.framework.draw.DrawObject;
import me.admund.framework.draw.DrawUtils;

/**
 * Created by admund on 2015-03-17.
 */
public class Background extends DrawObject {

    @Override
    public void draw(Batch batch, float parentAlpha) {
        DrawUtils.draw(batch, getSpriteList());
    }
}
