package me.admund.gmspaceinvaders.background;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import me.admund.framework.GameUtils;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.gmspaceinvaders.physics.WallElement;

/**
 * Created by admund on 2015-03-23.
 */
public class Wall extends Actor {

    private WallElement[] elementList = new WallElement[5];

    public void init() {
        //destroyAll();
        recreate();
    }

    private void recreate() {
        float startPosX = PhysicsWorld.BOX_SCREEN_WIDTH * .1f;
        for(int i=0; i<elementList.length; i++) {
            if(elementList[i] == null ||
                    elementList[i].canBeReuse())

            elementList[i] = (WallElement) GameUtils.world.getPhysicsObject(WallElement.class.getName());
            elementList[i].init(startPosX);
            getStage().addActor(elementList[i]);
            startPosX += PhysicsWorld.BOX_SCREEN_WIDTH * .2f;
        }
    }

    private void destroyAll() {
        for(int i=0; i<elementList.length; i++) {
            if(elementList[i] != null) {
                elementList[i].getInfo().setToReuse();
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        for(int i=0; i<elementList.length; i++) {
            elementList[i].draw(batch, parentAlpha);
        }
    }
}
