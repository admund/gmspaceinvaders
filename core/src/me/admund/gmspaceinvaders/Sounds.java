package me.admund.gmspaceinvaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

/**
 * Created by admund on 2015-03-24.
 */
public class Sounds {

    private Music main = null;
    private Sound laser = null;
    private Sound explosion = null;

    public void init() {
        main = Gdx.audio.newMusic(Gdx.files.internal("music/main.mp3"));
        main.setLooping(true);
        laser = Gdx.audio.newSound(Gdx.files.internal("music/laser.ogg"));
        explosion = Gdx.audio.newSound(Gdx.files.internal("music/explosion.wav"));
    }

    public void playMain() {
        main.play();
    }

    public void playLaser() {
        laser.play();
    }

    public void playExplosion() {
        explosion.play();
    }
}
