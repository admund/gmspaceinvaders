package me.admund.gmspaceinvaders.player;

import com.badlogic.gdx.utils.ObjectSet;
import me.admund.gmspaceinvaders.enemies.Enemy;

/**
 * Created by admund on 2015-03-19.
 */
public class HitCounter {
    private int hitCnt = 0;

    private ObjectSet<Enemy> set = new ObjectSet<Enemy>();

    public int getHitCnt() {
        return hitCnt;
    }

    public void addHit(Enemy enemy) {
        if(!set.contains(enemy)) {
            hitCnt++;
            set.add(enemy);
        }
    }

    public void clear() {
        hitCnt = 0;
        set.clear();
    }
}
