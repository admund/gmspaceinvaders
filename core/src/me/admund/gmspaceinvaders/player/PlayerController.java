package me.admund.gmspaceinvaders.player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.math.Vector2;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.framework.utils.MultiplicableValue;
import me.admund.framework.utils.TimeBoolean;

/**
 * Created by admund on 2015-01-11.
 */
public class PlayerController {
    private Player player = null;

    private float horizontalSpeed = 70f;

    private boolean goLeft = false;
    private boolean goRight = false;

    private TimeBoolean reloadTime = new TimeBoolean(true, 1f);//1f);

    private float MAX_X = PhysicsWorld.BOX_SCREEN_WIDTH - player.SIZE_Y;
    private float MIN_X = player.SIZE_Y;

    public PlayerController(Player player) {
        this.player = player;
        ((InputMultiplexer) Gdx.input.getInputProcessor()).addProcessor(1, new PlayerInput(this));
    }

    public void setMove(MoveEnum move, boolean start) {
        if (move == MoveEnum.LEFT) {
            goLeft = start;
        } else if (move == MoveEnum.RIGHT) {
            goRight = start;
        }
    }

    void shoot() {
        if(reloadTime.getValue()) {
            reloadTime.change();

            player.shoot();
        }
    }

    public float getReloadTime() {
        return reloadTime.getPercent();
    }

    public void update(float deltaTime) {
        updateVelocity();
        reloadTime.act(deltaTime);
    }

    public void reset() {
        goLeft = false;
        goRight = false;
    }

    protected void updateVelocity() {
        Vector2 pos = player.getPosition();
        if(pos.x >= MAX_X) {
            goRight = false;
        }
        if(pos.x <= MIN_X) {
            goLeft = false;
        }

        float tmp = goRight ? horizontalSpeed : 0f;
        tmp -= goLeft ? horizontalSpeed : 0f;
        player.getBody().setLinearVelocity(tmp, 0);
    }
}
