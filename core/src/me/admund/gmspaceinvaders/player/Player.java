package me.admund.gmspaceinvaders.player;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import me.admund.framework.GameUtils;
import me.admund.framework.draw.DrawUtils;
import me.admund.framework.draw.holders.SimpleSpriteHolder;
import me.admund.framework.physics.PhysicsObject;
import me.admund.framework.physics.PhysicsUtils;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.gmspaceinvaders.assets.AssetsList;
import me.admund.gmspaceinvaders.background.EngineFire;
import me.admund.gmspaceinvaders.enemies.Bang;
import me.admund.gmspaceinvaders.enemies.Enemy;
import me.admund.gmspaceinvaders.physics.CollisionFilters;
import me.admund.gmspaceinvaders.physics.Laser;

/**
 * Created by admund on 2015-01-11.
 */
public class Player extends PhysicsObject {
    public static final float SIZE_X = 9;
    public static final float SIZE_Y = 11;
    private static float START_POS_X = PhysicsWorld.BOX_SCREEN_WIDTH * .5f;
    private static float START_POS_Y = PhysicsWorld.BOX_SCREEN_HEIGHT * .1f;

    private boolean isDead = false;

    private PlayerController controller = null;

    private EngineFire engineFire = null;

    public Player() {
        controller = new PlayerController(this);
    }

    @Override
    public BodyDef getBodyDef() {
        BodyDef def = new BodyDef();
        def.active = true;
        def.fixedRotation = true;
        def.type = BodyDef.BodyType.DynamicBody;
        def.gravityScale = 0;
        return def;
    }

    @Override
    public FixtureDef getFixtureDef() {
        FixtureDef def = new FixtureDef();
        def.shape = PhysicsUtils.getDefaultPolygonShape();
        def.density = 1;//000f;
        def.filter.categoryBits = CollisionFilters.CATEGORY_PLAYER;
        def.filter.maskBits = CollisionFilters.MASK_PLAYER;
        return def;
    }

    public void init() {
        super.init();
        resetPlayer();
        setCurrentPos(START_POS_X, START_POS_Y);
        setSize(SIZE_X, SIZE_Y);
        setOrigin(Align.center);
        setSpriteHolder(new SimpleSpriteHolder(AssetsList.player));

        engineFire = new EngineFire();
        engineFire.init(this, SIZE_X * .25f, START_POS_Y - SIZE_Y * .85f);// * 1.5f);
        getStage().addActor(engineFire);

        controller.update(0);
    }

    @Override
    public void beginContact(Contact contact, boolean isObjectA) {
        Object obj = getContactedObject(contact, isObjectA);

        if(obj instanceof Enemy) {
            GameUtils.enemiesSpawner.autodestrucAllEnemies();
            getInfo().setToReuse();
            Bang.createBang(getPosition(), getStage());
            GameUtils.logic.stopGame();
        }
    }

    @Override
    public void endContact(Contact contact, boolean isObjectA) {

    }

    @Override
    public void act(float delta) {
        super.act(delta);
        controller.update(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        DrawUtils.draw(batch, getSpriteList());
    }

    void shoot() {
        HitCounter hitCounter = new HitCounter();

        Laser laser1 = (Laser) GameUtils.world.getPhysicsObject(Laser.class.getName());
        getStage().addActor(laser1);
        laser1.init(/*getX() + */SIZE_X * .1f, /*getY() + */SIZE_Y * .8f, hitCounter);
        //getStage().addActor(laser1);

        Laser laser2 = (Laser) GameUtils.world.getPhysicsObject(Laser.class.getName());
        getStage().addActor(laser2);
        laser2.init(/*getX() + */SIZE_X * .9f, /*getY() + */SIZE_Y * .8f, hitCounter);
        //getStage().addActor(laser2);

        GameUtils.sounds.playLaser();
    }

    public boolean isDead() {
        return isDead;
    }

    public float getReloadTime() {
        return controller.getReloadTime();
    }

    private void resetPlayer() {
        isDead = false;
        controller.reset();
    }
}
