package me.admund.gmspaceinvaders.player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.utils.Array;
import me.admund.framework.GameUtils;

/**
 * Created by admund on 2015-01-11.
 */
public class PlayerInput extends InputAdapter {

    private PlayerController playerController = null;

    public PlayerInput(PlayerController playerController) {
        this.playerController = playerController;
    }

    public Array<Touch> touchList = new Array<Touch>();

    @Override
    public boolean keyDown(int keycode) {
        boolean result = false;

        if(GameUtils.logic.isStarted()) {
            switch (keycode) {
                case Input.Keys.LEFT: {
                    playerController.setMove(MoveEnum.LEFT, true);
                    result = true;
                }
                break;

                case Input.Keys.RIGHT: {
                    playerController.setMove(MoveEnum.RIGHT, true);
                    result = true;
                }
                break;

                case Input.Keys.A: {
                    playerController.setMove(MoveEnum.LEFT, true);
                    result = true;
                }
                break;

                case Input.Keys.D: {
                    playerController.setMove(MoveEnum.RIGHT, true);
                    result = true;
                }
                break;

                case Input.Keys.SPACE: {
                    playerController.shoot();
                }
                break;
            }
        }

        if(keycode == Input.Keys.ENTER) {
            GameUtils.logic.restartGame();
        } else if(keycode == Input.Keys.Q) {
            GameUtils.logic.stopGame();
        }
        return result;
    }

    @Override
    public boolean keyUp(int keycode) {
        boolean result = false;
        switch (keycode) {
            case Input.Keys.LEFT: {
                playerController.setMove(MoveEnum.LEFT, false);
                result = true;
            }
            break;

            case Input.Keys.RIGHT: {
                playerController.setMove(MoveEnum.RIGHT, false);
                result = true;
            }
            break;

            case Input.Keys.A: {
                playerController.setMove(MoveEnum.LEFT, false);
                result = true;
            }
            break;

            case Input.Keys.D: {
                playerController.setMove(MoveEnum.RIGHT, false);
                result = true;
            }
            break;
        }
        return result;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if(GameUtils.logic.isStarted()) {
            if(screenY < Gdx.graphics.getHeight() * .8f) {
                playerController.shoot();
            } else if(screenX < Gdx.graphics.getWidth() * .5f) {
                playerController.setMove(MoveEnum.LEFT, true);
                touchList.add(new Touch(pointer, MoveEnum.LEFT));
            } else {
                playerController.setMove(MoveEnum.RIGHT, true);
                touchList.add(new Touch(pointer, MoveEnum.RIGHT));
            }
        } else {
            GameUtils.logic.restartGame();
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        for(int i=0; i<touchList.size; i++) {
            Touch tmp = touchList.get(i);
            if(tmp.pointerNr == pointer) {
                playerController.setMove(tmp.moveEnum, false);
                touchList.removeIndex(i);
                break;
            }
        }
        return false;
    }

    class Touch {
        Touch(int pointerNr, MoveEnum moveEnum) {
            this.pointerNr = pointerNr;
            this.moveEnum = moveEnum;
        }

        int pointerNr;
        MoveEnum moveEnum;
    }
}
